def get_new_hand(hand, result):
    '''
    //part 2//
    X means you need to lose, 
    Y means you need to draw, 
    Z means you need to win.
    '''
    if result == 1: # gotta lose
        if hand == 1:
            return 3
        return hand - 1

    if result == 2: # gotta draw
        return hand

    if result == 3: # gotta win
        if hand == 3:
            return 1
        return hand + 1 


def get_points(inp: str) -> int:
    '''
    A for Rock, B for Paper, and C for Scissors
    X for Rock, Y for Paper, and Z for Scissors
    1 for Rock, 2 for Paper, and 3 for Scissors
    '''
    hand = inp.strip().split(" ")
    inp1 = ord(hand[0]) - 64
    inp2 = (ord(hand[1]) - 87)
    inp2 = get_new_hand(inp1, inp2) # part 2
    if inp1 == inp2: # draw
        return 3 + inp2
    if inp1 > inp2 :
        if inp1 == 3 and inp2 ==1:
            return 6 + inp2
        return 0 + inp2
    if inp1 < inp2 :
        if inp1 == 1 and inp2 ==3:
            return 0 + inp2
        return 6 + inp2

with open("input.txt", "r") as f:
    lines = f.readlines()

sum = 0
for line in lines:
    sum += get_points(line)
    # print(f'score: {sum}')

print(f'score: {sum}')

