with open("input.txt", "r") as f:
    line = f.readline()

# part 1
for i, char in enumerate(line):
    if i < 3:
        continue
    if (char != line[i - 1]) and (char != line[i - 2 ]) and (char != line[i - 3]):
        if line[i - 1] != line[i - 2] and line[i - 1] != line[i - 3]:
            if line[i - 2] != line[i - 3]:
                print(f"i is {i + 1}, with chars {line[i - 3]}{char}{line[i - 2]}{line[i - 1]}")
                break

# part 2
def check_if_char_unique_in_last(buf):
    for ch in buf:
        if buf.count(ch) > 1:
            # print(f"{ch} has a dupe")
            return False
    return True

for i in range(14, len(line), 1):
    # print(line[i-14:i])
    if check_if_char_unique_in_last(line[i-14:i]):
        print(f"i is :{i}")
        break

    