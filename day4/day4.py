def check_coveraged(inp: str) -> bool:
    ranges = inp.strip().split(",")
    r1,r2 = ranges[0].split("-")
    r3,r4 = ranges[1].split("-")

    r1 = int(r1)
    r2 = int(r2)
    r3 = int(r3)
    r4 = int(r4)

    s1 = set(range(r1,r2+1))
    s2 = set(range(r3,r4+1))
    # print(s1)
    # print(s2)
    if s1.issubset(s2):
        print(f"{r1}-{r2} is subset of {r3}-{r4}")
        return 1
    if s2.issubset(s1):
        print(f"{r3}-{r4} is subset of {r1}-{r2}")
        return 1
    print(f"{r1}-{r2} and {r3}-{r4} dont contain each other")
    return 0

with open("input.txt", "r") as f:
    lines = f.readlines()

sum = 0
for line in lines:
    sum += check_coveraged(line)
    # print(f'is subset: {check_coveraged(line)}')

print(f'score: {sum}')

