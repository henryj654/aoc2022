def check_coveraged(inp: str) -> bool:
    ranges = inp.strip().split(",")
    r1,r2 = ranges[0].split("-")
    r3,r4 = ranges[1].split("-")

    r1 = int(r1)
    r2 = int(r2)
    r3 = int(r3)
    r4 = int(r4)

    s1 = set(range(r1,r2+1))
    s2 = set(range(r3,r4+1))
    s3 = s1 & s2
    # print(s3)
    if len(s3):
        return 1
    return 0

with open("input.txt", "r") as f:
    lines = f.readlines()

sum = 0
for line in lines:
    sum += check_coveraged(line)

print(f'score: {sum}')

