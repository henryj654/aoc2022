def get_point(inp: str) -> int:
    '''
    lowercase 1 - 26
    uppercase 27 - 52
    ''' 
    if inp.islower():
        return ord(inp) - 96
    return ord(inp) - 64 + 26

def get_dupe(line):
    num = int(len(line.strip()) / 2)
    comp1 = line[:num]
    comp2 = line[num:]
    for item in comp1:
        if item in comp2:
            return item

def get_badge(lines: list):
    for item in lines[0]:
        if (item in lines[1]) and (item in lines[2]):
            return item 

lines = []
with open("input.txt", "r") as f:
    lines = f.readlines()

sum = 0
group = []
for i in range(0, len(lines), 3) :
    group.append(lines[i])
    group.append(lines[i+1])
    group.append(lines[i+2])
    sum += get_point(get_badge(group))
    group.clear()

print(f'score: {sum}')

