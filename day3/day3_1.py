def get_point(inp: str) -> int:
    '''
    lowercase 1 - 26
    uppercase 27 - 52
    ''' 
    if inp.islower():
        return ord(inp) - 96
    return ord(inp) - 64 + 26

def get_dupe(line):
    num = int(len(line.strip()) / 2)
    comp1 = line[:num]
    comp2 = line[num:]
    for item in comp1:
        if item in comp2:
            return item

with open("input.txt", "r") as f:
    lines = f.readlines()

sum = 0
for line in lines:
    sum += get_point(get_dupe(line))

print(f'score: {sum}')

