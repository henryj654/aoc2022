with open("input1.txt", "r") as f:
    lines = f.readlines()

elf_list = []
sum = 0
for line in lines:
    try:
        sum += int(line)
    except ValueError:
        elf_list.append(sum)
        sum = 0
        continue
print(f'elf {elf_list.index(max(elf_list))}/{len(elf_list)} is carrying most calories : {max(elf_list)}')

#### part 2

elf_list.sort(reverse=True)
print(f'top 3 are carrying {elf_list[0] + elf_list[1] + elf_list[2]} calories')