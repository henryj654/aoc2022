with open("input.txt", "r") as f:
    lines = f.readlines()

# get height
sep = 0
for i, line, in enumerate(lines):
    if line == "\n":
        sep = i
        # print(f"empty line is {i+1}")
        break

# get number of stacks
nums = lines[sep-1].split("   ")
nums = [x.strip() for x in nums]
stacks = len(nums)-1

# print(stacks+1)

cargo = []

for i in range(stacks+1):
    cargo.append([])

for line in lines[:stacks]:
    i = 0 
    # print(f"getting from line {line}")
    for j, x in enumerate(range(4, len(line)+1, 4)):
        crate = line[i:x].strip("[] \n")
        i = x
        if crate != '':
            cargo[j].append(crate)
        # print(f"added crate is {crate}")

[x.reverse() for x in cargo]

# for col in cargo:
#     print(col)
# print(cargo)


for line in lines[sep+1:]:
    # print(line)
    cmd, count, fro, origin, to, target = line.strip().split(" ")
    count, origin, target = int(count), int(origin) - 1, int(target) - 1
    # print(count, origin, target)

    if count > 1: # part 2
        tmp = []
        for i, box in enumerate(range(0, count)):
            tmp.insert(0, cargo[origin].pop())
        cargo[target] = cargo[target] + tmp
    else:   # part 1
        for i, box in enumerate(range(0, count)):
            cargo[target].append(cargo[origin].pop())

res = ''
# print(f"result is:")
for col in cargo:
    res += col.pop()
    # print(col)
print(res)